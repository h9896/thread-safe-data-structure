package dictionary

import "testing"

func TestNew(t *testing.T) {
	dict := New()
	if dict.items != nil {
		t.Log("success")
	} else {
		t.Error("fail")
	}
}

func TestAddstringKeyValue(t *testing.T) {
	dict := New()
	err := dict.Add("A", "a")
	if err == nil {
		t.Log("success")
	} else {
		t.Error("fail")
	}
}

func TestAddintKeyValue(t *testing.T) {
	dict := New()
	err := dict.Add(1, 2)
	if err == nil {
		t.Log("success")
	} else {
		t.Error("fail")
	}
}

func TestGetValue(t *testing.T) {
	dict := New()
	err := dict.Add("A", "test")
	if err == nil {
		value, er := dict.GetValue("A")
		if er == nil {
			if value == "test" {
				t.Log("success")
			} else {
				t.Error("fail")
			}
		} else {
			t.Error("fail")
		}
	} else {
		t.Error("fail")
	}
}

func TestExist(t *testing.T) {
	dict := New()
	err := dict.Add("A", "test")
	if err == nil {
		if dict.Exist("A") {
			t.Log("success")
		} else {
			t.Error("fail")
		}
	} else {
		t.Error("fail")
	}
}

func TestRemove(t *testing.T) {
	dict := New()
	err := dict.Add("A", "test")
	if err == nil {
		if dict.Exist("A") {
			if dict.Remove("A") {
				t.Log("success")
			} else {
				t.Error("fail")
			}
		} else {
			t.Error("fail")
		}
	} else {
		t.Error("fail")
	}
}

func TestTryUpdate(t *testing.T) {
	dict := New()
	dict.Add("A", "test")
	old, _ := dict.GetValue("A")
	if dict.TryUpdate("A", "new Test") {
		new, _ := dict.GetValue("A")
		if new != old && new == "new Test" {
			t.Log("success")
		} else {
			t.Error("fail")
		}
	} else {
		t.Error("fail")
	}
}
