package dictionary

import (
	"errors"
	"log"
)

//New : initialize the dictionary.
func New() *Dictionary {
	dict := Dictionary{items: make(map[IKey]IValue)}
	return &dict
}

//Add : add the specified key and vale to dictionary.
func (dict *Dictionary) Add(key IKey, value IValue) error {
	dict.locker.Lock()
	defer dict.locker.Unlock()
	if dict.items == nil {
		err := errors.New("There is no elements. Please new the dictionary first")
		log.Fatal(err)
		return err
	}
	dict.items[key] = value
	return nil
}

//Remove : remove the specified key and value from dictionary.
func (dict *Dictionary) Remove(key IKey) bool {
	dict.locker.Lock()
	defer dict.locker.Unlock()
	if _, ok := dict.items[key]; ok {
		delete(dict.items, key)
		return true
	}
	return false
}

//GetValue : get the value associated with the specified key from dictionary.
func (dict *Dictionary) GetValue(key IKey) (IValue, error) {
	dict.locker.RLock()
	defer dict.locker.RUnlock()
	if _, ok := dict.items[key]; ok {
		return dict.items[key], nil
	}
	err := errors.New("There is no elements in dictionary")
	log.Fatal(err)
	return nil, err
}

//Exist : confirm whether the dictionary contains a specific key.
func (dict *Dictionary) Exist(key IKey) bool {
	dict.locker.RLock()
	defer dict.locker.RUnlock()
	if _, ok := dict.items[key]; ok {
		return true
	}
	return false
}

//TryUpdate : If the key is exist, the value will be updated. Otherwise, a new key and value will be added.
func (dict *Dictionary) TryUpdate(key IKey, value IValue) bool {
	dict.locker.Lock()
	defer dict.locker.Unlock()
	if dict.items == nil {
		return false
	}
	dict.items[key] = value
	return true
}

//Size : get the amount of elements in the dictionary.
func (dict *Dictionary) Size() int {
	dict.locker.RLock()
	defer dict.locker.RUnlock()
	return len(dict.items)
}

//Clear : Remove all keys and values from the dicitonary.
func (dict *Dictionary) Clear() {
	dict.locker.Lock()
	defer dict.locker.Unlock()
	dict.items = make(map[IKey]IValue)
}

//GetValuesArray : Get a slice of all the values in the dictionary.
func (dict *Dictionary) GetValuesArray() []IValue {
	dict.locker.RLock()
	defer dict.locker.RUnlock()
	values := []IValue{}
	for _, value := range dict.items {
		values = append(values, value)
	}
	return values
}

//GetKeysArray : Get a slice of all the keys in the dictionary.
func (dict *Dictionary) GetKeysArray() []IKey {
	dict.locker.RLock()
	defer dict.locker.RUnlock()
	keys := []IKey{}
	for key := range dict.items {
		keys = append(keys, key)
	}
	return keys
}
