package dictionary

import "sync"

//IKey : the key of dictionary
type IKey interface{}

//IValue : the value of dictionary
type IValue interface{}

//Dictionary : Represents a thread safe collection of key/value groups, which can be accessed by multiple threads in parallel.
type Dictionary struct {
	items  map[IKey]IValue
	locker sync.RWMutex
}
