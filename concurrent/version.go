package concurrent

// Version is the current concurrent framework's version.
const Version = "v1.1.1"
